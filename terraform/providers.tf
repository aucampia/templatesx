# https://www.terraform.io/docs/language/settings/index.html
# https://www.terraform.io/docs/language/expressions/version-constraints.html
terraform {
  required_providers {
    null = {
      source  = "hashicorp/null"
      version = "~> 3.2"
    }
    google-beta = {
      # https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs
      source  = "hashicorp/google-beta"
      version = "~> 5.2"
    }
    google = {
      # https://registry.terraform.io/providers/hashicorp/google/latest/docs
      source  = "hashicorp/google"
      version = "~> 5.2"
    }
    azurerm = {
      # https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs
      source  = "hashicorp/azurerm"
      version = "~> 3.76"
    }
    azuread = {
      # https://registry.terraform.io/providers/hashicorp/azuread/latest/docs
      source  = "hashicorp/azuread"
      version = "~> 2.43"
    }
    github = {
      # https://registry.terraform.io/providers/integrations/github/latest
      source  = "integrations/github"
      version = "~> 5.40"
    }
  }
  required_version = "~> 1.6"
}


provider "null" {
}

provider "google" {
}

provider "google-beta" {
}

provider "azurerm" {
  subscription_id = var.azure_subscription_id
  # tenant_id       = var.azure_tenant_id
  features {
  }
}

provider "azuread" {
  subscription_id = var.azure_subscription_id
  # tenant_id       = var.azure_tenant_id
}
