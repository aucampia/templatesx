# terraform template

- https://www.terraform-best-practices.com/
- https://developer.hashicorp.com/terraform/language/modules/develop/structure
- https://developer.hashicorp.com/terraform/cloud-docs/recommended-practices
- https://cloud.google.com/docs/terraform/best-practices-for-terraform
- https://spacelift.io/blog/terraform-best-practices


## Using

```bash
## Initial sync
rsync -av --no-owner --no-group --no-times --checksum --no-perms --ignore-existing \
    --exclude={.git,TEMPLATE-*.md,.terraform,.terraform.lock.hcl} \
    ~/sw/d/gitlab.com/aucampia/templatesx/terraform/ ./ --dry-run

## Diff summary ...
diff -u -r -q \
    --exclude={.git,TEMPLATE-*.md,.terraform,.terraform.lock.hcl} \
    ~/sw/d/gitlab.com/aucampia/templatesx/terraform/ ./

## vimdiff
diff -u -r \
    --exclude={.git,TEMPLATE-*.md,.terraform,.terraform.lock.hcl} \
    ~/sw/d/gitlab.com/aucampia/templatesx/terraform/ ./ \
    | sed -E -n 's,^diff.* /,vimdiff /,gp'

## diff
diff -u -r \
    --exclude={.git,TEMPLATE-*.md,.terraform,.terraform.lock.hcl} \
    ~/sw/d/gitlab.com/aucampia/templatesx/terraform/ ./
```

## Trash

```bash
docker compose run tflint bash

```
