# https://www.terraform.io/docs/language/values/variables.html

variable "secret" {
  type        = string
  sensitive   = true
  description = "A secret value"
}


variable "azure_subscription_id" {
  # tflint-ignore: terraform_unused_declarations
  type        = string
  description = "Azure subscription ID"
}

variable "azure_tenant_id" {
  # tflint-ignore: terraform_unused_declarations
  type        = string
  description = "Azure tenant ID"
}

variable "azure_location" {
  # tflint-ignore: terraform_unused_declarations
  type        = string
  description = "Azure location"
}
