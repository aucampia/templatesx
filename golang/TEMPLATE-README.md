# python-poetry template

## Using

```bash
## Initial sync
rsync -av --no-owner --no-group --no-times --checksum --no-perms --ignore-existing \
    --exclude={.git,LICENSE*,TEMPLATE-*.md,vendor,var} \
    ~/sw/d/gitlab.com/aucampia/templatesx/golang/ ./ --dry-run

## Diff summary ...
diff -u -r -q \
    --exclude={.git,LICENSE*,TEMPLATE-*.md,vendor,var} \
    ~/sw/d/gitlab.com/aucampia/templatesx/golang/ ./

## vimdiff
diff -u -r \
    --exclude={.git,LICENSE*,TEMPLATE-*.md,vendor,var} \
    ~/sw/d/gitlab.com/aucampia/templatesx/golang/ ./ \
    | sed -E -n 's,^diff.* /,vimdiff /,gp'

## diff
diff -u -r \
    --exclude={.git,LICENSE*,TEMPLATE-*.md,vendor,var} \
    ~/sw/d/gitlab.com/aucampia/templatesx/golang/ ./
```

```
find . \( -name .git -prune \) -o \( -type f -print0 \) \
  | xargs -0 -n1 -t sed -n 's,gitlab.com/aucampia/gotmpl,gitlab.com/aucampia/gotmpl,gp'
```
