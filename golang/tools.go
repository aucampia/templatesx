//go:build tools
// +build tools

package root

//nolint:golint
import (
	_ "github.com/cortesi/modd/cmd/modd" // toolchain
	_ "github.com/golang/mock/mockgen"   // toolchain
	_ "github.com/golang/mock/mockgen/model"
)
