# ...

```bash
go mod init gitlab.com/aucampia/templates/golang

```

```bash

tools
# dependencies
dnf install golang protobuf-compiler
go get -v github.com/ramya-rao-a/go-outline
go get -v golang.org/x/tools/gopls
go get -u golang.org/x/lint/golint
go get -v golang.org/x/tools/cmd/goimports
go get honnef.co/go/tools/cmd/staticcheck
go get google.golang.org/protobuf/cmd/protoc-gen-go google.golang.org/grpc/cmd/protoc-gen-go-grpc

#go get -u github.com/golang/protobuf/{proto,protoc-gen-go}
go get -u google.golang.org/grpc
go get github.com/uber/prototool/cmd/prototool


honnef.co/go/tools/cmd/staticcheck \
github.com/kisielk/errcheck \
golang.org/x/lint/golint \
```
