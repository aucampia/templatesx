# syntax = docker/dockerfile:experimental
# https://docs.docker.com/engine/reference/builder/
# https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/experimental.md
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
ARG base_image_registry=docker.io
ARG base_image_name=centos
ARG base_image_tag=8
ARG os_type=linux
ARG os_arch=x86_64
FROM ${base_image_registry}/${base_image_name}:${base_image_tag}

ARG cache_enabled=true
ARG force_rebuild=

########################################################################
# CONFIGURE CACHE
########################################################################

# https://dnf.readthedocs.io/en/latest/command_ref.html
# https://dnf.readthedocs.io/en/latest/conf_ref.html

RUN \
    1>&2 echo cache_enabled=${cache_enabled} && \
    1>&2 echo force_rebuild=${force_rebuild} && \
    if ${cache_enabled}; then \
        sed -E -i 's/^\s*metadata_expire\s*=.*$/metadata_expire=never/g' /etc/yum.repos.d/* && \
        echo 'metadata_expire=never' >> /etc/dnf/dnf.conf && \
        echo 'keepcache=True' >> /etc/dnf/dnf.conf && \
        echo 'fastestmirror=True' >> /etc/dnf/dnf.conf && \
        touch /var/cache/dockerfile.enabled && \
    true ; else \
        1>&2 echo "Not enabling cache for dnf" && \
    true ; fi && \
    true

########################################################################
# INSTALL busybox
########################################################################
# https://busybox.net/downloads/BusyBox.html

RUN \
    ls -l /var/cache/ && \
    { [ -e /var/cache/busybox-1.31.0-i686-uclibc ] || curl -o /var/cache/busybox-1.31.0-i686-uclibc -C - https://busybox.net/downloads/binaries/1.31.0-i686-uclibc/busybox; } && \
    cp /var/cache/busybox-1.31.0-i686-uclibc /usr/local/bin/busybox && \
    chown root:root /usr/local/bin/busybox && \
    chmod ugo+rx /usr/local/bin/busybox && \
    true

########################################################################
# INSTALL tools
########################################################################

RUN \
    dnf install -y \
        socat \
    && \
    true
