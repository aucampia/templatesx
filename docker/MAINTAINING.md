# ...

```bash
make oci-image-build oci_os_name=centos oci_os_version=7
make oci-image-build oci_os_name=centos oci_os_version=8
make oci-image-build oci_os_name=alpine oci_os_version=3
make oci-image-build oci_os_name=ubuntu oci_os_version=20.04
make oci-image-build oci_os_name=fedora oci_os_version=33


make oci-image-build oci_os_name=fedora oci_os_version=33
make oci-image-run-shell oci_os_name=fedora oci_os_version=33
make oci-image-build oci_os_name=fedora oci_os_version=33 USE_DOCKER=T
make oci-image-run-shell oci_os_name=fedora oci_os_version=33 USE_DOCKER=T
```

```bash
buildah logout docker.io
buildah login -u ${USER} docker.io
podman system prune -a
docker builder prune -af
```
