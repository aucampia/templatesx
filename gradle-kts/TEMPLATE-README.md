# python-poetry template

## Using

```bash
## Initial sync
rsync -av --no-owner --no-group --no-times --checksum --no-perms --ignore-existing \
    --exclude={.git,TEMPLATE-*.md,.gradle,build,.classpath,.project,.settings} \
    ~/d/gitlab.com/aucampia/templatesx/gradle-kts/ ./ --dry-run

## Diff summary ...
diff -u -r -q \
    --exclude={.git,TEMPLATE-*.md,.gradle,build,.classpath,.project,.settings} \
    ~/d/gitlab.com/aucampia/templatesx/gradle-kts/ ./

## vimdiff
diff -u -r \
    --exclude={.git,TEMPLATE-*.md,.gradle,build,.classpath,.project,.settings} \
    ~/d/gitlab.com/aucampia/templatesx/gradle-kts/ ./ \
    | sed -E -n 's,^diff.* /,vimdiff /,gp'

## diff
diff -u -r \
    --exclude={.git,TEMPLATE-*.md,.gradle,build,.classpath,.project,.settings} \
    ~/d/gitlab.com/aucampia/templatesx/gradle-kts/ ./
```

