# ...

```bash
poetry new --src --name python-poetry python-poetry
mv -v README.{rst,md}
poetry add --dev mypy pylint
```
