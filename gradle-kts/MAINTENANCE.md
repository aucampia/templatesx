# ...

```
./gradlew build --continuous
./gradlew run --stacktrace
./gradlew test --stacktrace
./gradlew run --stacktrace -PmainClassX=com.gitlab.aucampia.template.gradle.App
java -jar build/libs/aucampia-template-java-gradle.jar

./gradlew spotlessApply
./gradlew eclipse

./gradlew run --stacktrace -Dlog4j2.debug=true
./gradlew --no-build-cache cleanTest test -i
```

## CREATION

```bash
sdk install gradle
sdk upgrade

sdk use gradle 6.7.1

ls -ld ~/.sdkman/candidates/*/*

# rm -rf gradle gradlew* *.gradle.kts src/

gradle --version

gradle help --task init
gradle init --project-name aucampia-template-java-gradle --dsl kotlin --package com.gitlab.aucampia.template.gradle  --type java-application --test-framework junit
mv -v app/* .
rmdir app

# remove include("app") from settings.gradle.kts

# fix .gitignore

# vimdiff <(curl --silent -L https://github.com/github/gitignore/raw/master/Global/VisualStudioCode.gitignore) .gitignore
# vimdiff <(curl --silent -L https://github.com/github/gitignore/raw/master/Global/Eclipse.gitignore) .gitignore
# vimdiff <(curl --silent -L https://github.com/github/gitignore/raw/master/Java.gitignore) .gitignore
# vimdiff <(curl --silent -L https://github.com/github/gitignore/raw/master/Gradle.gitignore) .gitignore

# Install gradle buildship in eclipse
# Set plugin

# Export formatter profiles from eclipse :
#  https://stackoverflow.com/questions/1601793/how-do-i-modify-eclipse-code-formatting
#  https://stackoverflow.com/questions/21341314/eclipse-default-custom-formatter
```
